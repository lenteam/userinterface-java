package colMen.model;


import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnect {

    private final Logger log = Logger.getLogger(DbConnect.class);
    public Connection connect() {
        Connection conn = null;
        String databaseUrl = "jdbc:sqlite:colMen.db";
        try {
            conn = DriverManager.getConnection(databaseUrl);
        } catch (SQLException e) {
            log.error("Not connect to db! " + e);
        }

        return conn;
    }

}
