
package colMen.model.impl.sqlLite;


import colMen.model.DbConnect;
import colMen.model.Person;
import colMen.model.interfaces.IDAO;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class PersonDAOGetGSImpl implements IDAO<Map> {

    private DbConnect dbConnect = new DbConnect();
    private final Logger log = Logger.getLogger(PersonDAOGetGSImpl.class);

    @Override
    public Map<String, Integer> query() {

        Map<String, Integer> personsGS = new HashMap<>();

        try (Connection connection = dbConnect.connect();
             Statement stmtPerson = connection.createStatement();
             Statement stmtPersonPageRank = connection.createStatement();
             ResultSet rsPerson = stmtPerson.executeQuery("SELECT * FROM PERSONS")

        ) {

            while (rsPerson.next()) {
                ResultSet rsPersonPageRank = stmtPersonPageRank.executeQuery("SELECT * FROM PERSONPAGERANK where personid = \"" + rsPerson.getInt(1) + "\"");
                int countRank = 0;
                while (rsPersonPageRank.next()) {
                    countRank += rsPersonPageRank.getInt(3);
                }
                rsPersonPageRank.close();
                Person person = new Person();
                person.setName(rsPerson.getString(2));
                personsGS.put(person.getName(), countRank);

            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return personsGS;
    }
}

