
package colMen.model.impl.sqlLite;


import colMen.model.DbConnect;
import colMen.model.Site;
import colMen.model.interfaces.IDAO;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class SiteDAOGetNameImpl implements IDAO<Set> {

    private DbConnect dbConnect = new DbConnect();
    private final Logger log = Logger.getLogger(SiteDAOGetNameImpl.class);

    @Override
    public Set query() {

        Set<Site> sites = new HashSet<>();

        try (Connection connection = dbConnect.connect();
             Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM SITES")
        ) {

            while (rs.next()) {

                Site site = new Site();
                site.setName(rs.getString(2));
                sites.add(site);

            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return sites;
    }
}

