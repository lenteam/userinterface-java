
package colMen.model.impl.sqlLite;


import colMen.model.DbConnect;
import colMen.model.Person;
import colMen.model.interfaces.IDAO;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

public class PersonDAOGetDSImpl implements IDAO<Map> {

    private DbConnect dbConnect = new DbConnect();
    private final Logger log = Logger.getLogger(PersonDAOGetDSImpl.class);

    private String personName;
    private String siteName;
    private String dateFrom;
    private String dateTo;

    public PersonDAOGetDSImpl(String personName, String siteName, String dateFrom, String dateTo) {
        this.personName = personName;
        this.siteName = siteName;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    @Override
    public Map<String, Integer> query() {

        Map<String, Integer> personsDS = new TreeMap<>();

        try (Connection connection = dbConnect.connect();
             Statement stmtSites = connection.createStatement();
             Statement stmtPages = connection.createStatement();
             Statement stmtPersons = connection.createStatement();
             Statement stmtPersonsPageRank = connection.createStatement();
             ResultSet rsSites = stmtSites.executeQuery("SELECT * FROM SITES where name = \"" + siteName + "\"");
             ResultSet rsPerson = stmtPersons.executeQuery("SELECT * FROM PERSONS where name = \"" + personName + "\"");
             ResultSet rsPersonPageRank = stmtPersonsPageRank.executeQuery("SELECT * FROM PERSONPAGERANK where PERSONID = \"" + rsPerson.getInt(1) + "\"")


        ) {

            int countRank = 0;
            while (rsPersonPageRank.next()) {
                ResultSet rsPages = stmtPages.executeQuery("SELECT * FROM PAGES where SITEID = \"" + rsSites.getInt(1) + "\" and ID = \"" + rsPersonPageRank.getInt(2) + "\"and FOUNDDATETIME BETWEEN \"" + dateFrom + "\" and \"" + dateTo + "\"");
                if (rsPages.next()) {
                    Person person = new Person();
                    person.setName(rsPerson.getString(2));
                    if (!personsDS.containsKey(rsPages.getString(4))) {
                        countRank = 0;
                        countRank += rsPersonPageRank.getInt(3);
                    } else {
                        countRank += rsPersonPageRank.getInt(3);
                    }
                    personsDS.put(rsPages.getString(4), countRank);
                }

            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return personsDS;
    }
}

