
package colMen.model.impl.sqlLite;


import colMen.model.DbConnect;
import colMen.model.Person;
import colMen.model.interfaces.IDAO;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class PersonDAOGetNameImpl implements IDAO<Set> {

    private DbConnect dbConnect = new DbConnect();
    private final Logger log = Logger.getLogger(PersonDAOGetNameImpl.class);

    @Override
    public Set query() {

        Set<Person> persons = new HashSet<>();

        try (Connection connection = dbConnect.connect();
             Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM PERSONS")
        ) {

            while (rs.next()) {

                Person person = new Person();
                person.setName(rs.getString(2));
                persons.add(person);

            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return persons;
    }
}

