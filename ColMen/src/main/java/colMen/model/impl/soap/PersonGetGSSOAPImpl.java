
package colMen.model.impl.soap;


import colMen.model.DbConnect;
import colMen.model.Person;
import colMen.model.interfaces.IDAO;
import colMen.model.interfaces.ISOAP;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class PersonGetGSSOAPImpl implements ISOAP<Map> {

    private DbConnect dbConnect = new DbConnect();
    private final Logger log = Logger.getLogger(PersonGetGSSOAPImpl.class);

    @Override
    public Map<String, Integer> get() {

        Map<String, Integer> personsGS = new HashMap<>();


        HttpClient client = new DefaultHttpClient();
        // HttpGet request = new HttpGet("http://lenteam.sergvg.ru:8010/api/sites");
        HttpGet request = new HttpGet("http://lenteam.hldns.ru/colmenservice_soap.asmx/GetAllPersonStatistic?user=user&password=user");
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String line;
        try {
            Person person = null;
            while ((line = rd.readLine()) != null) {
                if (line.contains("<Name>") ) {
                    person = new Person();
                    person.setName(line.substring(line.indexOf("<Name>") + 6, line.indexOf("</Name>")));
                }
                if (line.contains("<sum")) {
                    personsGS.put(person.getName(), Integer.parseInt(line.substring(line.indexOf("_>") + 2, line.indexOf("</"))));
                }


            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return personsGS;
    }
}

