
package colMen.model.impl.soap;


import colMen.model.Site;
import colMen.model.interfaces.IDAO;
import colMen.model.interfaces.ISOAP;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class SiteGetNameSOAPImpl implements ISOAP<Set> {


    private final Logger log = Logger.getLogger(SiteGetNameSOAPImpl.class);

    @Override
    public Set get() {

        Set<Site> sites = new HashSet<>();

        HttpClient client = new DefaultHttpClient();
        // HttpGet request = new HttpGet("http://lenteam.sergvg.ru:8010/api/sites");
        HttpGet request = new HttpGet("http://lenteam.hldns.ru/colmenservice_soap.asmx/GetSites?user=user&password=user");
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String line;
        try {
            while ((line = rd.readLine()) != null) {
                if (line.contains("<Name>")) {
                    Site site = new Site();
                    site.setName(line.substring(line.indexOf("<Name>") + 6, line.indexOf("</Name>")));
                    sites.add(site);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sites;
    }
}

