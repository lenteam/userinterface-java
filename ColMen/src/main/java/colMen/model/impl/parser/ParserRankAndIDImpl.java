package colMen.model.impl.parser;


import colMen.model.interfaces.IParser;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ParserRankAndIDImpl implements IParser <Map<String, String>>{

    private String data;

    public ParserRankAndIDImpl(String data) {
        this.data = data;
    }

    @Override
    public Map<String, String> parse() {

        Map<String, String> outputData = new HashMap<>();
        StringBuilder line = new StringBuilder();
        line.append("{\"keyParamArray\":{\"KeyParam\":");
        line.append(data);
        line.append("}}");

        try {
            JSONObject json = new JSONObject(line.toString());


            JSONObject responseData = json.getJSONObject("keyParamArray");
            final JSONArray geodata = responseData.getJSONArray("KeyParam");
            final int n = geodata.length();
            for (int i = 0; i < n; ++i) {
                final JSONObject person = geodata.getJSONObject(i);
                outputData.put(person.getString("id"), person.getString("rank"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return outputData;

    }
}
