
package colMen.model.impl.rest;


import colMen.model.impl.parser.ParserNameAndIDImpl;
import colMen.model.Person;
import colMen.model.interfaces.IReST;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PersonGetNameReSTImpl implements IReST<Set> {


    private final Logger log = Logger.getLogger(PersonGetNameReSTImpl.class);

    @Override
    public Set get() {

        Set<Person> persons = new HashSet<>();
        Map<String, String> data = new HashMap<>();
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://lenteam.sergvg.ru:8010/api/persons");
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException e) {
            e.printStackTrace();
        }
       /* String line;
        try {
            while ((line = rd.readLine()) != null) {
                if (line.contains("name")) {
                    Person person = new Person();
                    person.setId(line.substring(line.indexOf("\"id\":") + 5, line.indexOf(",\"")));
                    person.setName(line.substring(line.indexOf("\"name\":") + 8, line.indexOf("\"}")));
                    persons.add(person);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        try {
            ParserNameAndIDImpl parserNameAndIDImpl = new ParserNameAndIDImpl(rd.readLine());
            data = parserNameAndIDImpl.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Map.Entry<String, String> dataForSite : data.entrySet()) {
            Person person = new Person();
            person.setId(dataForSite.getKey());
            person.setName(dataForSite.getValue());
            persons.add(person);
        }


        return persons;
    }
}

