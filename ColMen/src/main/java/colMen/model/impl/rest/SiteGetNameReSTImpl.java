
package colMen.model.impl.rest;


import colMen.model.impl.parser.ParserNameAndIDImpl;
import colMen.model.Site;
import colMen.model.interfaces.IParser;
import colMen.model.interfaces.IReST;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SiteGetNameReSTImpl implements IReST<Set> {


    private final Logger log = Logger.getLogger(SiteGetNameReSTImpl.class);

    @Override
    public Set get() {

        Set<Site> sites = new HashSet<>();
        Map<String, String> data = new HashMap<>();
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://lenteam.sergvg.ru:8010/api/sites");
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            IParser parser = new ParserNameAndIDImpl(rd.readLine());
            data = (Map<String, String>) parser.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Map.Entry<String, String> dataForSite : data.entrySet()) {
            Site site = new Site();
            site.setId(dataForSite.getKey());
            site.setName(dataForSite.getValue());
            sites.add(site);
        }

        /*String line;
        try {
            while ((line = rd.readLine()) != null) {
                if (line.contains("name")) {
                    Site site = new Site();
                    site.setId(line.substring(line.indexOf("\"id\":") + 5, line.indexOf(",\"")));
                    site.setName(line.substring(line.indexOf("\"name\":") + 8, line.indexOf("\"}")));
                    sites.add(site);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return sites;
    }
}

