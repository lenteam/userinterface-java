
package colMen.model.impl.rest;


import colMen.model.Person;
import colMen.model.impl.parser.ParserRankAndIDImpl;
import colMen.model.interfaces.IParser;
import colMen.model.interfaces.IReST;
import colMen.view.Controller;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


public class PersonGetGSReSTImpl implements IReST<Map<String, String>> {

    private final Logger log = Logger.getLogger(PersonGetGSReSTImpl.class);

    @Override
    public Map<String, String> get() {

        Map<String, String> personsGS = new HashMap<>();
        Map<String, String> data = new HashMap<>();

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://lenteam.sergvg.ru:8010/api/stat/persons/onsite/1");
        //HttpGet request = new HttpGet("http://lenteam.hldns.ru/colmenservice_soap.asmx/GetAllPersonStatistic?user=user&password=user");
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            IParser parser = new ParserRankAndIDImpl(rd.readLine());
            data = (Map<String, String>) parser.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Map.Entry<String, String> dataForPersons : data.entrySet()) {
            for (Person person : Controller.persons) {
                if (person.getId().equals(dataForPersons.getKey())) {
                    personsGS.put(person.getName(), dataForPersons.getValue());
                }
            }

        }


        return personsGS;
    }
}

