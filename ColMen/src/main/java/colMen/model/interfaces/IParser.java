package colMen.model.interfaces;


public interface IParser<T> {


    T parse();
}
