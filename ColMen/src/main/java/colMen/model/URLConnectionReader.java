package colMen.model;

//http://automation-remarks.com/java-rest-client/


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class URLConnectionReader {

    public static void main(String[] args) throws Exception {


        Map<String, String> names = new HashMap<>();
        HttpClient client = new DefaultHttpClient();
        //HttpGet request = new HttpGet("http://lenteam.sergvg.ru:8010/api/sites");
        HttpGet request = new HttpGet("http://lenteam.sergvg.ru:8010/api/stat/daily/1/1/2016-08-01/2016-08-02");
        //HttpGet request = new HttpGet("http://lenteam.hldns.ru/colmenservice_soap.asmx/GetAllPersonStatistic?user=user&password=user");
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder line = new StringBuilder();
        line.append("{\"keyParamArray\":{\"KeyParam\":");
        line.append(rd.readLine());
        line.append("}}");

        /*JSONObject json = new JSONObject(line.toString());


        JSONObject responseData = json.getJSONObject("keyParamArray");
        final JSONArray geodata = responseData.getJSONArray("KeyParam");
        final int n = geodata.length();
        for (int i = 0; i < n; ++i) {
            final JSONObject person = geodata.getJSONObject(i);
            names.put(person.getString("id"), person.getString("name"));
        }*/

        System.out.println(line);

    }
}
