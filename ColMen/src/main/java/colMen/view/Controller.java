
package colMen.view;


import colMen.model.Person;
import colMen.model.Site;
import colMen.model.impl.sqlLite.*;
import colMen.model.impl.rest.PersonGetGSReSTImpl;
import colMen.model.impl.rest.PersonGetNameReSTImpl;
import colMen.model.impl.rest.SiteGetNameReSTImpl;
import colMen.model.interfaces.IDAO;
import colMen.model.interfaces.IReST;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class Controller {


    @FXML
    private ComboBox siteGS;
    @FXML
    private ComboBox siteDS;
    @FXML
    private ComboBox personDS;
    @FXML
    private TableView tableGS;
    @FXML
    private TableView tableDS;
    @FXML
    private TableColumn nameGS;
    @FXML
    private TableColumn countNameGS;
    @FXML
    private TableColumn date;
    @FXML
    private TableColumn countNewPage;
    @FXML
    private DatePicker dateFrom;
    @FXML
    private DatePicker dateTo;

    public static Set<Site> sites;
    public static Set<Person> persons;


    @FXML
    private void initialize() {

        IReST getSites = new SiteGetNameReSTImpl();
        IReST getPersons = new PersonGetNameReSTImpl();
        sites = (Set<Site>) getSites.get();
        persons = (Set<Person>) getPersons.get();
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MONTH, 1);


        for (Site site : sites) {
            siteGS.getItems().add(site.getName());
            siteDS.getItems().add(site.getName());
        }
        for (Person person : persons) {
            personDS.getItems().add(person.getName());
        }
        siteGS.getSelectionModel().select(0);
        siteDS.getSelectionModel().select(0);
        personDS.getSelectionModel().select(0);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        dateFrom.setValue(LocalDate.of(year, month, day));
        dateTo.setValue(LocalDate.of(year, month, day));

        nameGS.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<ObservableList, Integer> param) {
                return new SimpleObjectProperty(param.getValue().get(0));
            }
        });

        countNameGS.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                return new SimpleStringProperty(param.getValue().get(1).toString());
            }
        });

        date.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<ObservableList, Integer> param) {
                return new SimpleObjectProperty(param.getValue().get(0));
            }
        });

        countNewPage.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                return new SimpleStringProperty(param.getValue().get(1).toString());
            }
        });

    }

    @FXML
    private void applyGS() {

        IReST getPersonGS = new PersonGetGSReSTImpl();
        Map<String, Integer> personGS = (Map<String, Integer>) getPersonGS.get();
        ObservableList<ObservableList> tableData = FXCollections.observableArrayList();


        for (Map.Entry<String, Integer> person : personGS.entrySet()) {
            ObservableList<Object> rowData = FXCollections.observableArrayList();
            rowData.add(person.getKey());
            rowData.add(person.getValue());
            tableData.add(rowData);
        }

        tableGS.setItems(tableData);

    }

    @FXML
    private void applyDS() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        IDAO getPersonDS = new PersonDAOGetDSImpl(personDS.getSelectionModel().getSelectedItem().toString(),
                siteDS.getSelectionModel().getSelectedItem().toString(),
                dateFrom.getValue().format(formatter),
                dateTo.getValue().format(formatter));
        Map<String, Integer> personDS = (Map<String, Integer>) getPersonDS.query();
        ObservableList<ObservableList> tableData = FXCollections.observableArrayList();


        for (Map.Entry<String, Integer> person : personDS.entrySet()) {
            ObservableList<Object> rowData = FXCollections.observableArrayList();
            rowData.add(person.getKey());
            rowData.add(person.getValue());
            tableData.add(rowData);
        }

        tableDS.setItems(tableData);

    }
}

